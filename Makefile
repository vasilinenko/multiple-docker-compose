restart: down up
up:
	cd app1 && make up
	cd app2 && make up
	cd app3 && make up
	cd gateway && make up
down:
	cd app1 && make down
	cd app2 && make down
	cd app3 && make down
	cd gateway && make down
