# Multiple project Docker Compose

## Init

##### Add in /etc/hosts

```bash
127.0.0.1 app1.local app2.local app3.local
```

##### Create network

```bash
docker network create xdebug
```

##### Get gateway IP
For linux:
```bash
docker network inspect xdebug | grep -w Gateway | awk '{print $2}' | awk -F\" '{print $2}'
```
IP for MacOS & Win:
```
host.docker.internal
```

##### Add IP to .env file in App1, App2, App3
```
XDEBUG_HOST=[Gateway IP]
```

##### Add xdebug settings to PHPStorm

[Example](./docs/xdebug.md)

## Start
```bash
make up
```

## Stop
```bash
make down
```

## Restart
```bash
make restart
```
