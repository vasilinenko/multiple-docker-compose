# Setup xdebug in PHPStorm

For example:

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/Add-Docker-Server.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/Languages-PHP72.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/Configure-remote-interpreter-docker.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/Configure-remote-interpreter-docker-compose.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/CLI-Interpreters-Docker-PHP72.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/Languages-PHP72-With-Docker.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/Languages-PHP-Debug.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/Xdebug271-validate-result.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/PHP-Server-with-Mapping.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/Run-Debug-Configuration.png)

![Alt text](https://blog.denisbondar.com/wp-content/uploads/2018/10/Active-Break-Poing.png)
